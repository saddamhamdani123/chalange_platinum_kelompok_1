<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Seller</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>47dc01b1-9475-43dc-b997-916e0ffcafab</testSuiteGuid>
   <testCaseLink>
      <guid>448f2dea-cc03-496b-aa15-46e702703f61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step_Description/User_Seller/Sel.002.002 - user want to add item that it cannot add unnamed items</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e202366-8f0f-4d72-a3f2-ed657fc3827f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step_Description/User_Seller/Sel.002.003 - user want to add item that it cannot add uncategory item</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b18ff68-fafa-422c-ac11-68e3b3e7d0a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Step_Description/User_Seller/Sel.002.007 - notife buyer who are interested in the item</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
