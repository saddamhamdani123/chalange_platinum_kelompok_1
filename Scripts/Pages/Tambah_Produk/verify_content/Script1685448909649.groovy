import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.verifyElementVisible(findTestObject('Page_Tambah_Produk/button_kembali'))

WebUI.verifyElementVisible(findTestObject('Page_Tambah_Produk/textfield_nama_produk'))

WebUI.verifyElementVisible(findTestObject('Page_Tambah_Produk/textfield_harga_produk'))

WebUI.verifyElementVisible(findTestObject('Page_Tambah_Produk/select_kategori'))

WebUI.verifyElementVisible(findTestObject('Page_Tambah_Produk/textfield_deskripsi'))

WebUI.verifyElementVisible(findTestObject('Page_Tambah_Produk/button_preview'))

WebUI.verifyElementVisible(findTestObject('Page_Tambah_Produk/button_terbitkan'))

